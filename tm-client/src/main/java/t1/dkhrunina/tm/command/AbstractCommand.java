package t1.dkhrunina.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.model.ICommand;
import t1.dkhrunina.tm.api.service.IServiceLocator;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }
//
//    @Nullable
//    public abstract Role[] getRoles();
//
//    @NotNull
//    public IAuthEndpoint getAuthEndpointClient() {
//        return serviceLocator.getAuthEndpoint();
//    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? (", " + argument) : argument;
        if (hasDescription) result += hasName || hasArgument ? (": " + description) : description;
        return result;
    }

}