package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change project status by index.";

    @Override
    public void execute() {
        System.out.println("[Change project status by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken(), index, status);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}