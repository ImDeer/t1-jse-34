package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectCompleteByIdRequest;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[Complete project by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken(), id);
        getProjectEndpoint().completeProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}