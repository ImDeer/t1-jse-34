package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.project.ProjectFindOneByIndexRequest;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[Show project by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(getToken(), index);
        @Nullable final Project project = getProjectEndpoint().findOneByIndex(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}