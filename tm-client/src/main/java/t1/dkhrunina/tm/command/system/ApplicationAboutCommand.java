package t1.dkhrunina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.system.ServerAboutRequest;
import t1.dkhrunina.tm.dto.response.system.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("\n[APPLICATION NAME]");
        System.out.println("Name: " + response.getApplicationName());

        System.out.println("\n[AUTHOR]");
        System.out.println("Name: " + response.getName());
        System.out.println("Email: " + response.getEmail());

        System.out.println("\n[GIT]");
        System.out.println("Branch: " + response.getGitBranch());
        System.out.println("Commit ID: " + response.getGitCommitId());
        System.out.println("Commit time: " + response.getGitCommitTime());
        System.out.println("Commit message: " + response.getGitCommitMessage());
        System.out.println("Committer: " + response.getGitCommitter());
        System.out.println("Committer email: " + response.getGitCommitterEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}