package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskListByProjectIdRequest;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-show-by-project";

    @NotNull
    private static final String DESCRIPTION = "Show tasks by project id.";

    @Override
    public void execute() {
        System.out.println("[Task list by project id]");
        System.out.println("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final List<Task> tasks = getTaskEndpoint().listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}