package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskStartByIndexRequest;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() {
        System.out.println("[Start task by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken(), index);
        getTaskEndpoint().startTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}