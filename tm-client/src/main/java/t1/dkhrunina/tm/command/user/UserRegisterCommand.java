package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.user.UserRegisterRequest;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-register";

    @NotNull
    private static final String DESCRIPTION = "Register new user.";

    @Override
    public void execute() {
        System.out.println("[User registration]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter email: ");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegisterRequest request = new UserRegisterRequest(login, password, email);
        @Nullable final User user = getUserEndpoint().registerUser(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}