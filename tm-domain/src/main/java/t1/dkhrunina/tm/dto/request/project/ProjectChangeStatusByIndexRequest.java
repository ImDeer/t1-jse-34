package t1.dkhrunina.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;
import t1.dkhrunina.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        super(token);
        this.index = index;
        this.status = status;
    }

}