package t1.dkhrunina.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest(
            @Nullable final String token,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.name = name;
        this.description = description;
    }

}