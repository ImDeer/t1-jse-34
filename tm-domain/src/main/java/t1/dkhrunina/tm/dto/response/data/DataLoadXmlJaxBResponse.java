package t1.dkhrunina.tm.dto.response.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class DataLoadXmlJaxBResponse extends AbstractResponse {

}