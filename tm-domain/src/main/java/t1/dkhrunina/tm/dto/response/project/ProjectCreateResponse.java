package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCreateResponse extends AbstractResultResponse {

    @Nullable
    private Project project;

    public ProjectCreateResponse(@Nullable final Project project) {
        this.project = project;
    }

}