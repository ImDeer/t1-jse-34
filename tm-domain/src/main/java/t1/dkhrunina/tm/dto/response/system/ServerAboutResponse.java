package t1.dkhrunina.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class ServerAboutResponse extends AbstractResponse {

    private String applicationName;

    private String email;

    private String name;

    private String gitBranch;

    private String gitCommitId;

    private String gitCommitTime;

    private String gitCommitMessage;

    private String gitCommitter;

    private String gitCommitterEmail;

}