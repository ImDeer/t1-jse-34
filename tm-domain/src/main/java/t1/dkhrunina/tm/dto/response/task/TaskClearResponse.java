package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearResponse extends AbstractResultResponse {

}