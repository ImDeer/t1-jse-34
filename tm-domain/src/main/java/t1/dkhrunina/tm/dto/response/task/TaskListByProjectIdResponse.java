package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdResponse extends AbstractResultResponse {

    @NotNull
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

}