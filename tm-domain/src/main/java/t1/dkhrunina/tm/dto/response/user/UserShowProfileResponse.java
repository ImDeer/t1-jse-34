package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserShowProfileResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    public UserShowProfileResponse(@Nullable final User user) {
        this.user = user;
    }

}