package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockResponse extends AbstractResponse {

}