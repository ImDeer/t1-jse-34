package t1.dkhrunina.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USUAL("Regular user"), ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}