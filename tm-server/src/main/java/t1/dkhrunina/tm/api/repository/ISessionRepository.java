package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}