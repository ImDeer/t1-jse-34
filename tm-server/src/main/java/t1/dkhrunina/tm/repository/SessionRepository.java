package t1.dkhrunina.tm.repository;

import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}