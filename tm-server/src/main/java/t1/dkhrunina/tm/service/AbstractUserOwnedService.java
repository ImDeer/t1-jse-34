package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.IUserOwnedRepository;
import t1.dkhrunina.tm.api.service.IUserOwnedService;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public M add(
            @NotNull final String userId,
            @Nullable final M model
    ) {
        if (model == null) return null;
        return repository.add(userId, model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<M> comparator
    ) {
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M remove(
            @NotNull final String userId,
            @Nullable final M model
    ) {
        if (model == null) return null;
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    public M removeById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public M removeByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(
            @NotNull final String userId,
            @Nullable final Sort sort
    ) {
        if (sort == null) return repository.findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

}